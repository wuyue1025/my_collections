package wy.test.time;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

import junit.framework.TestCase;



public class TimeTest extends TestCase{
	private SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	/** 
	* @Title: testSysTime 
	* @Description: 以指定格式输出时间
	* @return void    返回类型
	* @author （吴岳） 
	* @throws
	* @date 2016年9月12日 下午8:05:25 
	* @version V1.0   
	*/
	public void testSysTime() {		
		System.out.println(simpleDateFormat.format(new Date()));		
	}
	
	/** 
	* @Title: testTimestamp 
	* @Description: 时间戳
	* @return void    返回类型
	* @author （吴岳） 
	* @throws
	* @date 2018年9月13日 下午4:03:18 
	* @version V1.0   
	*/
	public void testTimestamp() {
		Timestamp timestamp=new Timestamp(System.currentTimeMillis());
		
		System.out.println(timestamp.toString());
	}
}
