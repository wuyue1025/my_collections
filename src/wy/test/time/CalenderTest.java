package wy.test.time;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import junit.framework.TestCase;

public class CalenderTest extends TestCase {
	
	/** 
	* @Title: testCalender 
	* @Description: calender输出
	* @return void    返回类型
	* @author （吴岳） 
	* @throws
	* @date 2016年9月10日 下午7:57:16 
	* @version V1.0   
	*/
	public void testCalender() {
		SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Calendar calendar=Calendar.getInstance();
		System.out.println(simpleDateFormat.format(calendar.getTime()));
	}
	
	/** 
	* @Title: testFirstDay 
	* @Description: 取指定日期
	* @return void    返回类型
	* @author （吴岳） 
	* @throws
	* @date 2018年9月12日 下午8:05:06 
	* @version V1.0   
	*/
	public void testFirstDay() {
		SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Calendar calendar=Calendar.getInstance();
		
		//获取当前月份第一天
		calendar.setTime(new Date());
		calendar.add(Calendar.MONTH, 0);
		calendar.set(Calendar.DAY_OF_MONTH, 1);
		
		System.out.println(simpleDateFormat.format(calendar.getTime()));
	}
}
