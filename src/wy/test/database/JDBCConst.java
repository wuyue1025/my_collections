package wy.test.database;

public class JDBCConst {
	
	
	/** 
	* @Fields MYSQL_DRIVER : mysql JDBC驱动  适用于mysql8.0以下版本
	*/ 
	public static final String MYSQL_DRIVER_OLD="com.mysql.jdbc.Driver";	
	/** 
	* @Fields MYSQL_DRIVER_NEW : mysql JDBC驱动  适用于mysql8.0及以上版本
	*/ 
	public static final String MYSQL_DRIVER_NEW="com.mysql.cj.jdbc.Driver";
	/** 
	* @Fields ORACLE_DRIVER : oracle JDBC驱动   需要ojdbc5.jar
	*/ 
	public static final String ORACLE_DRIVER="oracle.jdbc.driver.OracleDriver";
	/** 
	* @Fields SQL_SERVER_DRIVER :sql_server JDBC驱动  需要sqljdbc4.jar
	*/ 
	public static final String SQL_SERVER_DRIVER="com.microsoft.sqlserver.jdbc.SQLServerDriver";
}
