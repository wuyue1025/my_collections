package wy.test.database.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import junit.framework.TestCase;
import wy.test.database.JDBCConst;

public class JDBCTest extends TestCase{		
	
	private static final String dirver=JDBCConst.MYSQL_DRIVER_NEW;//数据库驱动
	private static final String url="jdbc:mysql://localhost:3306/mylife?useSSL=false";//数据库连接
	private static final String username="root";//数据库用户名
	private static final String password="123123";//数据库密码
	
	private Connection connection=null;
	private PreparedStatement preparedStatement=null;
	private ResultSet resultSet=null;
	/** 
	* @Title: testMySqlQuery 
	* @Description: 记录JDBC查询
	* @return void    返回类型
	* @author （吴岳） 
	* @throws
	* @date 2016年9月12日 下午5:48:46 
	* @version V1.0   
	*/
	public void testMySqlQuery() {				
		try {
			//1、加载驱动
			Class.forName(dirver);
			
			//2、创建数据库连接
			connection=DriverManager.getConnection(url, username, password);	
			
			//3、创建SQL语句对象
			String sql="SELECT * FROM jdbc_test a WHERE a.username=?";
			preparedStatement=connection.prepareStatement(sql);//使用PreparedStatement可防止SQL注入
			preparedStatement.setString(1, "david");
			
			//4、执行数据库语句
			resultSet=preparedStatement.executeQuery();
			
			//5、处理结果集
			while(resultSet.next()) {
				System.out.println("id:"+resultSet.getLong("id"));
				System.out.println("username:"+resultSet.getString("username"));
				System.out.println("password:"+resultSet.getString("password"));
			}
			
			//6、关闭连接（就近关闭，即后开先关）
			resultSet.close();
			preparedStatement.close();
			connection.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/** 
	* @Title: testMySqlUpdate 
	* @Description: 记录JDBC更新
	* @return void    返回类型
	* @author （吴岳） 
	* @throws
	* @date 2016年9月12日 下午6:24:02 
	* @version V1.0   
	*/
	public void testMySqlUpdate() {
		try {
			Class.forName(dirver);
			
			connection=DriverManager.getConnection(url, username, password);
			
			String sql="update jdbc_test set password=? where username=?";
			preparedStatement=connection.prepareStatement(sql);
			preparedStatement.setString(1, "123456");
			preparedStatement.setString(2, "david");
			
			System.out.println(preparedStatement.executeUpdate()+" rows updated");					
			if(resultSet!=null) {
				resultSet.close();
			}			
			preparedStatement.close();
			connection.close();
		} catch (Exception e) {
			
			e.printStackTrace();
		}
	}
}
