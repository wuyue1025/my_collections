package wy.test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

import org.apache.commons.io.FileUtils;

import junit.framework.TestCase;

public class FileTest extends TestCase {
	
	
	/** 
	* @Title: testCreate 
	* @Description: 创建文件（文件夹存在）
	* @return void    返回类型
	* @author （吴岳） 
	* @throws
	* @date 2016年9月13日 下午4:43:00 
	* @version V1.0   
	*/
	public void testCreate() {
		File file=new File("resources/testFile.txt");
		try {
			if(!file.exists()) {
				file.createNewFile();
			}else {
				System.out.println("File already exists");
			}		
		} catch (Exception e) {			
			e.printStackTrace();
		}
	}
	
	/** 
	* @Title: testCreateWithoutDir 
	* @Description: 创建文件（文件夹不存在）
	* @return void    返回类型
	* @author （吴岳） 
	* @throws
	* @date 2016年9月13日 下午4:43:41 
	* @version V1.0   
	*/
	public void testCreateWithoutDir() {
		File file=new File("/resources/file/testFile.txt");
		try {
			if(!file.exists()) {
				if(!file.getParentFile().exists()) {
					System.out.println(file.getParentFile().getCanonicalPath());
					file.getParentFile().mkdirs();
				}
				file.createNewFile();
			}else {
				System.out.println("File already exists");
			}
		} catch (Exception e) {			
			e.printStackTrace();
		}
	}
	
	/** 
	* @Title: testDelete 
	* @Description: 删除文件
	* @return void    返回类型
	* @author （吴岳） 
	* @throws
	* @date 2016年9月13日 下午4:47:03 
	* @version V1.0   
	*/
	public void testDelete() {
		File file=new File("resources/testFile.txt");
		try {
			if(file.exists()) {
				file.delete();
			}
		} catch (Exception e) {			
			e.printStackTrace();
		}
	}
	
	/** 
	* @Title: testWrite 
	* @Description: 文件写入
	* @return void    返回类型
	* @author （吴岳） 
	* @throws
	* @date 2016年9月13日 下午4:49:00 
	* @version V1.0   
	*/
	public void testWrite() {
		File file=new File("resources/testFile.txt");
		try {
			//读原文件
			StringBuffer stringBuffer=new StringBuffer();
			stringBuffer.append(FileUtils.readFileToString(file, "utf-8"));		
			//设置写入的内容
			for(int i=1;i<=20;i++) {
				stringBuffer.append("这是第 "+i+" 行\n");
			}
			//写入文件
			FileOutputStream fileOutputStream=new FileOutputStream(file);
			fileOutputStream.write(stringBuffer.toString().getBytes("utf-8"));
			//关闭文件流
			fileOutputStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/** 
	* @Title: testReadFile 
	* @Description: 读文件
	* @return void    返回类型
	* @author （吴岳） 
	* @throws
	* @date 2016年9月13日 下午5:02:15 
	* @version V1.0   
	*/
	public void testReadFile() {
		try {
			File file=new File("resources/testFile.txt");
			//1、判断文件是否存在
			if(file.exists()) {
				//2、判断文件路径是否为目录
				if(file.isDirectory()) {
					throw new Exception("File "+ file.getName()+" is a directory!");
				}
				//3、判断文件是否可读
				if(!file.canRead()) {
					throw new Exception("File "+file.getName()+" unreadable!");
				}
				//4、读取文件到文件流
				InputStream InputStream=new FileInputStream(file);
				InputStreamReader inputStreamReader=new InputStreamReader(InputStream,StandardCharsets.UTF_8);
				BufferedReader bufferedReader=new BufferedReader(inputStreamReader);
				StringBuffer stringBuffer=new StringBuffer();
				
				String lines="";
				while((lines=bufferedReader.readLine())!=null) {
					stringBuffer.append(lines+"\n");				
				}
				
				//5、关闭相关流
				bufferedReader.close();				
				inputStreamReader.close();
				InputStream.close();
				System.out.println(stringBuffer.toString());
			}else {
				throw new Exception("File "+file.getName()+" is not exist!");
			}		
		} catch (Exception e) {			
			e.printStackTrace();
		}
		
		
	}
}
