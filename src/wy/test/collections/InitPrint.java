package wy.test.collections;

public class InitPrint {
	static {
		System.out.println("start");
	}
	public InitPrint() {
		System.out.println("init");
	}
	
	public static void main(String[] args) {		
		Second first=new Second();
		
	}
}
class First{
	public static String first=null;	
	static {
		first="hello world";
		System.out.println("first static code block");		
	}
	public First() {
		first="byebye";
		System.out.println("first Code Construct Code Block.");
	}
	public First(String name) {
		System.out.println(name);
	}
}
class Second extends First{
	static {
		System.out.println("second static");
	}
	public Second() {
		System.out.println("second code block.");
	}
	public Second(String name) {
		new First(name);
		System.out.println("name:"+name);
	}
}