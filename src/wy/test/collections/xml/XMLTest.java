package wy.test.collections.xml;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.charset.StandardCharsets;
import java.util.Iterator;
import java.util.List;

import javax.swing.plaf.synth.SynthScrollBarUI;

import org.apache.commons.io.FileUtils;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;

import junit.framework.TestCase;

public class XMLTest extends TestCase{
	private static final String path="resources/testXML.xml";		
	
	
	/** 
	* @Title: testCreateXML 
	* @Description: 创建一个XML文档
	* @return void    返回类型
	* @author （吴岳） 
	* @throws
	* @date 2018年9月19日 下午3:18:54 
	* @version V1.0   
	*/
	public void testCreateXML() {
		File file=new File(path);
		try {
			if(!file.exists()) {
				file.createNewFile();
				System.out.println("done");
			}else {
				System.out.println("file exist");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/** 
	* @Title: testDeleteXMLFile 
	* @Description: 删除XML文档
	* @return void    返回类型
	* @author （吴岳） 
	* @throws
	* @date 2018年9月19日 下午3:19:26 
	* @version V1.0   
	*/
	public void testDeleteXMLFile() {
		File file=new File(path);
		try {
			if(file.exists()) {
				file.delete();
				System.out.println("done");
			}else {
				System.out.println("file not exist.");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/** 
	* @Title: testWrite 
	* @Description: 构造XML
	* @return void    返回类型
	* @author （吴岳） 
	* @throws
	* @date 2018年9月19日 下午3:21:29 
	* @version V1.0   
	*/
	public void testWrite() {
		Document document=DocumentHelper.createDocument();
		Element root=document.addElement("UserInfo");
		root.addElement("Name").setText("David");
		root.addElement("Age").setText("23");
		root.addElement("Address").setText("浙江-杭州");
		
		
		OutputFormat outputFormat=new OutputFormat(document.asXML());//格式化XML
		outputFormat.setEncoding("UTF-8");//设置字符编码
		outputFormat.setNewlines(true);//设置换行
		outputFormat.setIndent(true);//设置缩进
		outputFormat.setIndentSize(4);//设置缩进大小
		
		try {
			FileOutputStream fileOutputStream=new FileOutputStream(new File(path));//创建文件输出流
			XMLWriter xmlWriter=new XMLWriter(fileOutputStream, outputFormat);
			xmlWriter.write(document);//写入XML文档
			
			xmlWriter.close();
			fileOutputStream.close(); 
		} catch (Exception e) {			
			e.printStackTrace();
		}
	}
	
	/** 
	* @Title: testRead 
	* @Description: 读XML
	* @return void    返回类型
	* @author （吴岳） 
	* @throws
	* @date 2018年9月19日 下午3:55:31 
	* @version V1.0   
	*/
	public void testRead() {
		try {
			String xmlString=FileUtils.readFileToString(new File(path),StandardCharsets.UTF_8);
			Document document=DocumentHelper.parseText(xmlString);
			Element root=document.getRootElement();
			
			long startTime=System.currentTimeMillis();
			Iterator<Element> iterator=root.elementIterator();
			while(iterator.hasNext()) {
				Element node=(Element)iterator.next();
				System.out.println(node.getName()+" : "+node.getStringValue());
			}
			long endTime=System.currentTimeMillis();
			System.out.println(endTime-startTime);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
