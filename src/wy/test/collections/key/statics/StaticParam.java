package wy.test.collections.key.statics;

public class StaticParam {
	private static int age=20;
	public static void main(String[] args) {
		new StaticParam().getAge();//输出20
	}
	
	public void getAge() {
		int age=21;//内部对象，与this无关
		System.out.println(this.age);//this 关键字不可用于static方法中
	}
}
