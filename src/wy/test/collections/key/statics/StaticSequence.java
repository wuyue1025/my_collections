package wy.test.collections.key.statics;
/*
 * 多个static块会按定义的顺序执行
 * */
public class StaticSequence {
	static {
		System.out.println("first static block");//1
	}
	
	public static void main(String[] args) {
		
	}
	static {
		System.out.println("second static bolck");//2
	}
}
