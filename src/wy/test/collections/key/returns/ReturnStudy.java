package wy.test.collections.key.returns;

public class ReturnStudy {
	public static void getInfo(boolean flag) {
		if(flag) {
			System.out.println("hello world");
		}else {
			return;
		}
		System.out.println("done");
	}
	public static void main(String[] args) {
		getInfo(false);
	}
}
