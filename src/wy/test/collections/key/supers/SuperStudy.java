package wy.test.collections.key.supers;

public class SuperStudy {
	public static void main(String[] args) {
		new GrandSon().printName();
	}
}
class Father{
	public static String name="daivd";
	public Father() {
		System.out.println("Father Construct");
	}
	
	
	public void printName() {
		System.out.println(name);
	}
	
	public void printInfo() {
		System.out.println("hello world");
	}
}
class Son extends Father{
	public Son() {
		super();
	}
	
	public void printName() {
		System.out.println("Son");		
		super.printName();
		printInfo();
	}

}
class GrandSon extends Son{
	public GrandSon() {
		super();
	}
	public void printName() {
		printInfo();
	}
}
