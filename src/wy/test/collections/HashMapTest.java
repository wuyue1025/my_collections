package wy.test.collections;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

import junit.framework.TestCase;

public class HashMapTest extends TestCase {
	
	/** 
	* @Title: testHashMapPrint 
	* @Description: HashMap遍历
	* @return void    返回类型
	* @author （吴岳） 
	* @throws
	* @date 2016年9月01日 下午8:08:31 
	* @version V1.0   
	*/
	public void testHashMapPrint() {
		HashMap hashMap=new HashMap();
		hashMap.put("name", "david");
		hashMap.put("password","123123");
		hashMap.put("id", 123);
		
		Iterator<Entry> iterator=hashMap.entrySet().iterator();
		while(iterator.hasNext()) {
			Entry entry=iterator.next();
			System.out.println(entry.getKey()+"   "+entry.getValue());
		}					
	}
	
	/** 
	* @Title: testLinkedHashMapPrint 
	* @Description: LinkedHashMap遍历，有序的HashMap，默认以输入顺序输出
	* @return void    返回类型
	* @author （吴岳） 
	* @throws
	* @date 2018年9月13日 下午3:53:13 
	* @version V1.0   
	*/
	public void testLinkedHashMapPrint() {
		LinkedHashMap linkedHashMap=new LinkedHashMap();
		linkedHashMap.put("name", "david");
		linkedHashMap.put("password", "123");
		linkedHashMap.put("age", 100);
		Iterator<Entry> iterator=linkedHashMap.entrySet().iterator();
		while(iterator.hasNext()) {
			Entry entry=iterator.next();
			System.out.println(entry.getKey()+"  "+entry.getValue());
		}
	}
}
