package wy.test.interaction;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

import junit.framework.TestCase;

public class URLTest extends TestCase{
	
	/** 
	* @Title: testURL 
	* @Description: URL
	* @return void    返回类型
	* @author （吴岳） 
	* @throws
	* @date 2018年9月16日 下午1:20:05 
	* @version V1.0   
	*/
	public void testURL() {
		try {
			URL baidu=new URL("http://www.baidu.com");
			
			System.out.println(baidu.getProtocol());//通信协议
			System.out.println(baidu.getHost());//主机（IP/域名）
			System.out.println(baidu.getPort());//如果没有指定端口号，根据协议不同使用默认端口。此时getPort()方法的返回值为 -1
			System.out.println(baidu.getPath());
		} catch (MalformedURLException e) {			
			e.printStackTrace();
		}
	}
	
	/** 
	* @Title: testURLOperate 
	* @Description: URL操作，获取URL页面返回
	* @return void    返回类型
	* @author （吴岳） 
	* @throws
	* @date 2018年9月16日 下午1:22:47 
	* @version V1.0   
	*/
	public void testURLOperate() {
		try {
			URL url=new URL("http://www.baidu.com");
			
			InputStream inputStream=url.openStream();
			InputStreamReader inputStreamReader=new InputStreamReader(inputStream,"gbk");
			BufferedReader bufferedReader=new BufferedReader(inputStreamReader);
			StringBuffer stringBuffer=new StringBuffer();
			
			String lines="";
			while((lines=bufferedReader.readLine())!=null) {
				stringBuffer.append(lines).append("\n");
			}
			System.out.println(stringBuffer.toString());
			
			bufferedReader.close();
			inputStreamReader.close();
			inputStream.close();
		} catch (Exception e) {			
			e.printStackTrace();
		}
	}
}
