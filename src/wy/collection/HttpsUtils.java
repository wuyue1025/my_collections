package wy.collection;


import org.apache.http.*;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.util.EntityUtils;

import wy.consts.NormalConst;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Map;
import java.util.Iterator;
import java.util.List;


public class HttpsUtils {
	
	private HttpsUtils() {
		super();
	}
	
	private static final String CHARSET="utf-8";
	private static final Log log=LogFactory.getLog(HttpsUtils.class);
	
    private static final String HTTP = "http";
    private static final String HTTPS = "https";
    private static SSLConnectionSocketFactory sslsf = null;
    private static PoolingHttpClientConnectionManager cm = null;
    private static SSLContextBuilder builder = null;
    static {
        try {
            builder = new SSLContextBuilder();
            // 全部信任 不做身份鉴定
            builder.loadTrustMaterial(null, new TrustStrategy() {
                @Override
                public boolean isTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {
                    return true;
                }
            });
            sslsf = new SSLConnectionSocketFactory(builder.build(), new String[]{"SSLv2Hello", "SSLv3", "TLSv1", "TLSv1.2"}, null, NoopHostnameVerifier.INSTANCE);
            Registry<ConnectionSocketFactory> registry = RegistryBuilder.<ConnectionSocketFactory>create()
                    .register(HTTP, new PlainConnectionSocketFactory())
                    .register(HTTPS, sslsf)
                    .build();
            cm = new PoolingHttpClientConnectionManager(registry);
            cm.setMaxTotal(200);//max connection
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }
    
	/** 
	* @Title: doGet 
	* @Description: http/https+json  get请求
	* @param host 目标地址
	* @param path 目标方法（可为空）
	* @param querys 请求参数
	* @param headers 请求头
	* @param charset 请求编码
	* @return    入参
	* @return String    返回类型
	* @author （wuyue3） 
	* @throws
	* @date 2018年7月27日 下午3:08:12 
	* @version V1.0   
	*/
    public static String doGet(String host,String path,Map<String, String> querys,Map<String, String> headers,String charset) {
    	String result="";
    	CloseableHttpClient httpClient=null;
    	try {
			httpClient=getHttpClient();
			HttpGet httpGet=new HttpGet(buildUrl(host, path, querys));
			
			if(headers!=null&&headers.size()>0) {
				Iterator<Map.Entry<String, String>> iterator=headers.entrySet().iterator();
				while(iterator.hasNext()) {
					Map.Entry<String, String> entry=iterator.next();
					httpGet.addHeader(entry.getKey(), entry.getValue());
				}
			}
			
			HttpResponse response=httpClient.execute(httpGet);
			if(response!=null) {
				HttpEntity httpEntity=response.getEntity();
				if(httpEntity!=null) {
					result=EntityUtils.toString(httpEntity,charset);
				}
			}
		} catch (Exception e) {
			log.error(e.getMessage());	
			return null;
		}    	    	
    	return result;
    }
    

    /** 
    * @Title: doPost 
    * @Description: http/https+json post请求
    * @param host  目标地址（IP+端口）
    * @param path  目标方法
    * @param querys 请求参数
    * @param headers 请求头
    * @param charset 编码
    * @return    入参
    * @return String    返回类型
    * @author （wuyue3） 
    * @throws
    * @date 2018年8月1日 下午7:00:22 
    * @version V1.0   
    */
    public static String doPost(String host,String path,Map<String, String> querys,Map<String, String> headers,String charset) {
    	String result="";
    	CloseableHttpClient httpClient=null;
    	List list = new ArrayList<>();
    	try {
    		httpClient=getHttpClient();
    		HttpPost httpPost=new HttpPost(buildUrl(host, path, querys));
    		
			if(headers!=null&&headers.size()>0) {
				Iterator<Map.Entry<String, String>> iterator=headers.entrySet().iterator();
				while(iterator.hasNext()) {
					Map.Entry<String, String> entry=iterator.next();
					httpPost.addHeader(entry.getKey(), entry.getValue());
				}				
			}
			
			if(querys!=null&&querys.size()>0) {
				
				Iterator<Map.Entry<String, String>> iterator=querys.entrySet().iterator();
				while(iterator.hasNext()) {
					Map.Entry<String,String> entry=iterator.next();
					list.add(new BasicNameValuePair(entry.getKey(),entry.getValue()));
				}
			}
			
			if(!list.isEmpty()) {				
				UrlEncodedFormEntity urlEncodedFormEntity=new UrlEncodedFormEntity(list,CHARSET);
				httpPost.setEntity(urlEncodedFormEntity);
			}
			
			HttpResponse response=httpClient.execute(httpPost);
			if(response!=null) {
				HttpEntity httpEntity=response.getEntity();
				if(httpEntity!=null) {
					result=EntityUtils.toString(httpEntity,charset);
				}
			}
		} catch (Exception e) {
			log.error(e.getMessage());
			return null;
		}
    	return result;
    }
    
    
    /** 
    * @Title: getHttpClient 
    * @Description: 适用于https请求
    * @return
    * @throws 
    * @return CloseableHttpClient    返回类型
    * @author （wuyue3） 
    * @throws
    * @date 2018年7月29日 下午12:27:06 
    * @version V1.0   
    */
    public static CloseableHttpClient getHttpClient() {        
        return HttpClients.custom()
                .setSSLSocketFactory(sslsf)
                .setConnectionManager(cm)
                .setConnectionManagerShared(true)
                .build();
    }
    /** 
	* @Title: buildUrl 
	* @Description: 构建URL
	* @param host 目标地址
	* @param path 目标方法（可为空）
	* @param querys 发送参数
	* @return
	* @throws UnsupportedEncodingException    入参
	* @return String    返回类型
	* @author （wuyue3） 
	* @throws
	* @date 2018年7月27日 下午2:55:33 
	* @version V1.0   
	*/
	private static String buildUrl(String host, String path, Map<String, String> querys) throws UnsupportedEncodingException {
    	StringBuilder sbUrl = new StringBuilder();
    	sbUrl.append(host);
    	if (!StringUtils.isBlank(path)) {
    		sbUrl.append(path);
        }
    	if (!querys.isEmpty()) {
    		StringBuilder sbQuery = new StringBuilder();
        	for (Map.Entry<String, String> query : querys.entrySet()) {
        		if (0 < sbQuery.length()) {
        			sbQuery.append("&");
        		}
        		if (StringUtils.isBlank(query.getKey()) && !StringUtils.isBlank(query.getValue())) {
        			sbQuery.append(query.getValue());
                }
        		if (!StringUtils.isBlank(query.getKey())) {
        			sbQuery.append(query.getKey());
        			if (!StringUtils.isBlank(query.getValue())) {
        				sbQuery.append("=");
        				sbQuery.append(URLEncoder.encode(query.getValue(), CHARSET));
        			}        			
                }
        	}
        	if (0 < sbQuery.length()) {
        		sbUrl.append("?").append(sbQuery);
        	}
        }
    	
    	return sbUrl.toString();
    }
	
	/** 
	* @Title: isConnectedNetwork 
	* @Description: 测试网络连通性
	* @return    入参
	* @return boolean    返回类型
	* @author （wuyue） 
	* @throws
	* @date 2018年8月20日 下午3:49:17 
	* @version V1.0   
	*/
	public static boolean isConnectedNetwork() {
		Runtime runtime=Runtime.getRuntime();
		try {
			Process process=runtime.exec(NormalConst.InternetConst.CMD_PING+NormalConst.InternetConst.IP_BAIDU);
			StringBuilder stringBuilder=transRuntimeResult(process);
			if(stringBuilder.toString().length()>0&&stringBuilder.toString().indexOf(NormalConst.InternetConst.TTL)>=0) {
				return true;
			}
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return false;
	}
	
	/** 
	* @Title: isConnectedLink 
	* @Description: 测试域名连通性
	* @param domain
	* @return    入参
	* @return boolean    返回类型
	* @author （wuyue） 
	* @throws
	* @date 2018年8月20日 下午3:50:06 
	* @version V1.0   
	*/
	public static boolean isConnectedLink(String domain) {
		Runtime runtime=Runtime.getRuntime();
		try {
			Process process=runtime.exec(NormalConst.InternetConst.CMD_PING+domain);
			StringBuilder stringBuilder=transRuntimeResult(process);
			if(stringBuilder.toString().length()>0&&stringBuilder.toString().indexOf(NormalConst.InternetConst.TTL)>=0) {
				return true;
			}
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return false;
	}
	private static StringBuilder transRuntimeResult(Process process) throws IOException{
		if(process.getInputStream()!=null) {
			InputStream inputStream=process.getInputStream();
			InputStreamReader inputStreamReader=new InputStreamReader(inputStream);
			BufferedReader bufferedReader=new BufferedReader(inputStreamReader);
			String line=null;
			StringBuilder stringBuilder=new StringBuilder();
			while((line=bufferedReader.readLine())!=null) {
				stringBuilder.append(line);
			}
			return stringBuilder;
		}
		return new StringBuilder();
	}
}