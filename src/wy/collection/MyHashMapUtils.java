package wy.collection;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MyHashMapUtils {
	private MyHashMapUtils() {
		super();
	}
	
	/** 
	* @Title: transMap2List 
	* @Description: Map对象转List 
	* @param map
	* @return    入参
	* @return List<Map.Entry<String, Object>>    返回类型
	* @author （wuyue） 
	* @throws
	* @date 2018年8月18日 下午12:32:40 
	* @version V1.0   
	*/
	public static List<Map.Entry<String, Object>> transMap2List(Map<String, Object> map){
		List<Map.Entry<String, Object>> list=new ArrayList<>();
		if(map!=null&&map.size()>0) {
			for(Map.Entry<String, Object> entry:map.entrySet()) {
				list.add(entry);
			}
		}
		return list;
	}
}
