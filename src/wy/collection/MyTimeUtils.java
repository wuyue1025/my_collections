package wy.collection;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;


public class MyTimeUtils {
	
	private MyTimeUtils() {
		super();
	}
	
	/** 
	* @Fields yyyyMM : 201808 
	*/ 
	public static final String YYYYMM="yyyyMM";
	/** 
	* @Fields yyyyMMdd :  20180816 
	*/ 
	public static final String YYYYMMDD="yyyyMMdd";
	/** 
	* @Fields yyyyMMddhh : 2018081603
	*/ 
	public static final String YYYYMMDDHH="yyyyMMddhh";
	/** 
	* @Fields yyyyMMddhhmm : 201808160330
	*/ 
	public static final String YYYYMMDDHHMM="yyyyMMddhhmm";
	
	/** 
	* @Fields yyyyMMddhhmmss : 20180816033108 
	*/ 
	public static final String YYYYMMDDHHMMSS="yyyyMMddhhmmss";

	/** 
	* @Title: getSysTime 
	* @Description: 按格式获取当前系统时间 
	* @param timeFormate
	* @return    入参
	* @return String    返回类型
	* @author （wuyue） 
	* @throws
	* @date 2018年8月16日 下午3:20:00 
	* @version V1.0   
	*/
	public static String getSysTime(String timeFormate) {
		
		if(timeFormate!=null&&timeFormate.trim().length()>0) {
			SimpleDateFormat simpleDateFormat=new SimpleDateFormat(timeFormate);
			return simpleDateFormat.format(new Date());
		}		
		return null;
	}
	
	/** 
	* @Title: getTimestampNow 
	* @Description: 获取当前系统时间戳类型 
	* @return    入参
	* @return Timestamp    返回类型
	* @author （wuyue） 
	* @throws
	* @date 2018年8月16日 下午3:32:43 
	* @version V1.0   
	*/
	public static Timestamp getTimestampNow() {
		return new Timestamp(System.currentTimeMillis());
	}
}
