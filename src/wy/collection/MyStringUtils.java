package wy.collection;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import net.sourceforge.pinyin4j.PinyinHelper;
import net.sourceforge.pinyin4j.format.HanyuPinyinCaseType;
import net.sourceforge.pinyin4j.format.HanyuPinyinOutputFormat;
import net.sourceforge.pinyin4j.format.HanyuPinyinToneType;
import net.sourceforge.pinyin4j.format.HanyuPinyinVCharType;
import net.sourceforge.pinyin4j.format.exception.BadHanyuPinyinOutputFormatCombination;

/** 
* @ClassName: MyStringUtils 
* @Description: String处理类 
* @author (wuyue)  
* @date 2018年5月21日 上午11:06:31 
* @version V1.0 
*/
public class MyStringUtils {
	
	private MyStringUtils() {
		super();
	}
	
	private static final Log log=LogFactory.getLog(MyStringUtils.class);
	/** 
	* @Title: isEmpty 
	* @Description: 是否为空
	* @param str
	* @return    入参
	* @return boolean    返回类型
	* @author （wuyue） 
	* @throws
	* @date 2018年5月21日 下午2:26:27 
	* @version V1.0   
	*/
	public static boolean isEmpty(String str) {		
		return str!=null&&!"".equals(str.trim())&&str.length()>0;
	}
	
	 /** 
	* @Title: getPingYin 
	* @Description: 获取汉语拼音
	* @param src
	* @return    入参
	* @return String    返回类型
	* @author （Internet） 
	* @throws
	* @date 2018年5月25日 下午5:54:38 
	* @version V1.0   
	*/
	public static String getPingYin(String src) {  
	        char[] t1 = null;  
	        t1 = src.toCharArray();  
	        String[] t2 = new String[t1.length];  
	        HanyuPinyinOutputFormat t3 = new HanyuPinyinOutputFormat();  
	        t3.setCaseType(HanyuPinyinCaseType.LOWERCASE);  
	        t3.setToneType(HanyuPinyinToneType.WITHOUT_TONE);  
	        t3.setVCharType(HanyuPinyinVCharType.WITH_V);  
	        String t4 = "";  
	        int t0 = t1.length;  
	        try {  
	            for (int i = 0; i < t0; i++) {  
	                // 判断是否为汉字字符  
	                if (java.lang.Character.toString(t1[i]).matches("[\\u4E00-\\u9FA5]+")) {  
	                    t2 = PinyinHelper.toHanyuPinyinStringArray(t1[i], t3);  
	                    t4 += t2[0];  
	                } else {  
	                    t4 += java.lang.Character.toString(t1[i]);  
	                }  
	            }  
	            return t4;  
	        } catch (BadHanyuPinyinOutputFormatCombination e1) {  
	            log.error(e1.getMessage());
	        }  
	        return t4;  
	    }  
	
}
