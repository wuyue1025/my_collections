package wy.collection;

import java.util.Random;

public class MyMathUtils {
	private MyMathUtils() {
		super();
	}
	
	
	/** 
	* @Title: getNumber 
	* @Description: 获取指定长度随机数
	* @param count 随机数长度
	* @return    入参
	* @return String    返回类型
	* @author （wuyue） 
	* @throws
	* @date 2018年8月17日 下午6:09:25 
	* @version V1.0   
	*/
	public static String getNumber(int count) {
		Random random=new Random();
		StringBuilder stringBuilder=new StringBuilder();
		for(int i=0;i<count;i++) {
			stringBuilder.append(random.nextInt(10));
		}
		while(stringBuilder.toString().startsWith("0")) {
			stringBuilder.replace(0, 1, String.valueOf(random.nextInt(10)));
		}
		return stringBuilder.toString();
	}
}
