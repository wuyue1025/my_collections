package wy.collection;

import java.util.regex.Pattern;

import wy.consts.RegexConst;

public class RegexUtils {
	private RegexUtils() {
		super();
	}
	
	/** 
	* @Title: isPhone 
	* @Description: 校验手机号码
	* @param str 国内11位手机号码
	* @return    入参
	* @return boolean    返回类型
	* @author （wuyue） 
	* @throws
	* @date 2018年5月21日 上午11:31:43 
	* @version V1.0   
	*/
	public static boolean isPhone(String str) {
		return Pattern.compile(RegexConst.PHONE).matcher(str).matches();
	}
	/** 
	* @Title: containsChinese 
	* @Description: 校验字符串是否包含英文 
	* @param str
	* @return    入参
	* @return boolean    返回类型
	* @author （wuyue） 
	* @throws
	* @date 2018年5月21日 上午11:36:39 
	* @version V1.0   
	*/
	public static boolean containsChinese(String str) {
		return Pattern.compile(RegexConst.CHINESE).matcher(str).find();		
	}
	/** 
	* @Title: isEmail 
	* @Description: 校验是否为邮箱
	* @param str
	* @return    入参
	* @return boolean    返回类型
	* @author （wuyue） 
	* @throws
	* @date 2018年5月21日 上午11:37:58 
	* @version V1.0   
	*/
	public static boolean isEmail(String str) {
		return Pattern.compile(RegexConst.E_MAIL).matcher(str).matches();
	}
	
	/** 
	* @Title: isChinese 
	* @Description: 是否为中文
	* @param str
	* @return    入参
	* @return boolean    返回类型
	* @author （wuyue） 
	* @throws
	* @date 2018年5月25日 下午5:49:52 
	* @version V1.0   
	*/
	public static boolean isChinese(String str) {
		return Pattern.compile(RegexConst.IS_CHINESE).matcher(str).matches();		
	}
}
