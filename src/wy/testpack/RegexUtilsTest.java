package wy.testpack;

import junit.framework.TestCase;
import wy.collection.RegexUtils;

public class RegexUtilsTest extends TestCase{
	public void testIsEmail() {
		System.out.println(RegexUtils.isEmail("2515280439@163.comas"));
		System.out.println(RegexUtils.isEmail(""));
		System.out.println(RegexUtils.isEmail("asd@qwe"));
	}
	public void testIsPhone() {
		System.out.println(RegexUtils.isPhone("13773900155"));
		System.out.println(RegexUtils.isPhone("13951456692"));
		System.out.println(RegexUtils.isPhone("17552757636"));
		System.out.println(RegexUtils.isPhone("1311111111"));
	}
	public void testContainChinese() {
		System.out.println(RegexUtils.containsChinese("asd���asd"));
	}
	public void testisChinese() {
		System.out.println(RegexUtils.isChinese("���"));
	}
}
