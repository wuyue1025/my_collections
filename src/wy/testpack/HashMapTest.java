package wy.testpack;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import junit.framework.TestCase;
import wy.collection.MyHashMapUtils;

public class HashMapTest extends TestCase {
	/** 
	* @Title: testTrans2List 
	* @Description: 测试Map转List
	* @return void    返回类型
	* @author （wuyue） 
	* @throws
	* @date 2018年8月18日 下午2:24:39 
	* @version V1.0   
	*/
	public void testTrans2List() {
		HashMap<String, Object> hashMap=new HashMap<String,Object>();
		hashMap.put("name", "david");
		hashMap.put("pwd", "123123");
		hashMap.put("age", null);
		
		List<Map.Entry<String, Object>> list=MyHashMapUtils.transMap2List(hashMap);
		for(Map.Entry<String, Object> entry:list) {
			System.out.println(entry.getKey()+"            "+entry.getValue());
		}
	}
}
