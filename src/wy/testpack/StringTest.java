package wy.testpack;

import junit.framework.TestCase;
import net.sourceforge.pinyin4j.PinyinHelper;
import wy.collection.MyStringUtils;

public class StringTest extends TestCase{
	public void testGetPinyin() {
		System.out.println(MyStringUtils.getPingYin("你hi好余"));
	}
	
	/** 
	* @Title: testPinYin4j 
	* @Description: 输出汉语拼音
	* @return void    返回类型
	* @author （wuyue） 
	* @throws
	* @date 2018年5月25日 下午5:48:32 
	* @version V1.0   
	*/
	public void testPinYin4j() {
		String[] pinyin= PinyinHelper.toHanyuPinyinStringArray('单');
		for(int i=0;i<pinyin.length;i++) {
			System.out.println(pinyin[i]);
		}
	}
}
