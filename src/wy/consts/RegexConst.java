package wy.consts;

/** 
* @ClassName: RegexConst 
* @Description: 正则表达式规则
* @author (wuyue3)  
* @date 2018年5月21日 上午11:28:21 
* @version V1.0 
*/
public class RegexConst {
	private RegexConst() {
		super();
	}
	
	/** 
	* @Fields phone : 国内手机号码验证
	*/ 
	public static final String PHONE="^1[3|4|5|7|8][0-9]\\d{8}$";
	
	/** 
	* @Fields containChinese : 包含中文
	*/ 
	public static final String CHINESE="[\\u4e00-\\u9fa5]";
	/** 
	* @Fields containChinese : 全中文校验
	*/ 
	public static final String IS_CHINESE="[\\u4e00-\\u9fa5]+";
	
	/** 
	* @Fields email : 邮箱
	*/ 
	public static final String E_MAIL="^\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*$";

	
}
